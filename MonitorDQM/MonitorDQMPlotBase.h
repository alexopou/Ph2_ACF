/*!
  \file                  MonitorDQMPlotBase.h
  \brief                 base class to create and fill monitoring histograms
  \author                Fabio Ravera, Lorenzo Uplegger
  \version               1.0
  \date                  6/5/19
  Support:               email to fabio.ravera@cern.ch
*/

#ifndef __MonitorDQMPlotBASE_H__
#define __MonitorDQMPlotBASE_H__

#include "Parser/DetectorMonitorConfig.h"
#include "Parser/FileParser.h"
#include "RootUtils/GraphContainer.h"
#include "RootUtils/RootContainerFactory.h"
#include "Utils/Container.h"

#include <memory>
#include <string>
#include <unistd.h>
#include <vector>

#include <TAxis.h>
#include <TDatime.h>
#include <TGraph.h>

class DetectorDataContainer;
class DetectorContainer;
class TFile;

/*!
 * \class MonitorDQMPlotBase
 * \brief Base class for monitoring histograms
 */
class MonitorDQMPlotBase
{
  public:
    /*!
     * constructor
     */
    MonitorDQMPlotBase() { ; }

    /*!
     * destructor
     */
    virtual ~MonitorDQMPlotBase() {}

    /*!
     * \brief Book histograms
     * \param theDetectorStructure : Container of the Detector structure
     */
    virtual void book(TFile* outputFile, DetectorContainer& theDetectorStructure, const DetectorMonitorConfig& detectorMonitorConfig) = 0;

    /*!
     * \brief Book histograms
     * \param configurationFileName : xml configuration file
     */
    virtual bool fill(std::string& inputStream) = 0;

    /*!
     * \brief SAve histograms
     * \param outFile : ouput file name
     */
    virtual void process() = 0;

    /*!
     * \brief Book histograms
     * \param configurationFileName : xml configuration file
     */
    virtual void reset(void) = 0;

  protected:
    uint32_t getTimeStampForRoot(time_t rawTime)
    {
        struct tm* timeinfo = localtime(&rawTime);
        char       timeStampString[80];
        strftime(timeStampString, sizeof(timeStampString), TIME_FORMAT, timeinfo);
        std::string tmpTime{std::to_string(1) + timeStampString};

        TDatime rootTime(tmpTime.c_str());
        return rootTime.Convert();
    }

    void bookImplementer(TFile*                   theOutputFile,
                         const DetectorContainer& theDetectorStructure,
                         DetectorDataContainer&   dataContainer,
                         GraphContainer<TGraph>&  graphContainer,
                         const std::string&       type,
                         const char*              XTitle = nullptr,
                         const char*              YTitle = nullptr)
    {
        graphContainer.fTheGraph->GetXaxis()->SetTimeDisplay(1);
        graphContainer.fTheGraph->GetXaxis()->SetNdivisions(503);
        graphContainer.fTheGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);
        graphContainer.fTheGraph->GetXaxis()->SetTimeOffset(0);
        if(XTitle != nullptr) graphContainer.fTheGraph->GetXaxis()->SetTitle(XTitle);
        if(YTitle != nullptr)
        {
            graphContainer.setNameTitle("DQM_" + std::string{YTitle}, "DQM_" + std::string{YTitle});
            graphContainer.fTheGraph->GetYaxis()->SetTitle(YTitle);
        }
        graphContainer.fTheGraph->SetMarkerStyle(20);
        graphContainer.fTheGraph->SetMarkerSize(0.8);

        if(type == "chip")
            RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, dataContainer, graphContainer);
        else if(type == "opto")
            RootContainerFactory::bookOpticalGroupHistograms(theOutputFile, theDetectorStructure, dataContainer, graphContainer);
    }
};

#endif
