*--------------------------------------------------------------------------------
* Control Registers
*--------------------------------------------------------------------------------
* RegName                                       Block   Addr    Defval  Value
*--------------------------------------------------------------------------------
MUX_CTRL                                        0x00    0x00    0x00    0x00
MISC_CTRL                                       0x02    0x44    0x02    0x02
SLVS_PADS_CONFIG                                0x02    0x45    0x18    0x1F
*--------------------------------------------------------------------------------
* Front-end configuration 
*--------------------------------------------------------------------------------
FE_CONFIG                                       0x02    0x40    0xFF    0x00
FE_ENABLE                                       0x02    0x41    0xFF    0xFF
*--------------------------------------------------------------------------------
* Stub packing configuration 
*--------------------------------------------------------------------------------
BX0_ALIGN_CONFIG                                0x02    0x42    0x80    0x80
EXT_BX0_DELAY                                   0x02    0x43    0x16    0x08
*--------------------------------------------------------------------------------
* Hit data packing configuration 
*--------------------------------------------------------------------------------
L1_DATA_TIMEOUT_VALUE0                          0x02    0x46    0x00    0x00
L1_DATA_TIMEOUT_VALUE1                          0x02    0x47    0x04    0x0F
*--------------------------------------------------------------------------------
* Word-aligner configuration 
*--------------------------------------------------------------------------------
CALIB_PATTERN0                                  0x02    0x4A    0xA1    0x7A
CALIB_PATTERN1                                  0x02    0x4B    0xA1    0xBC
CALIB_PATTERN2                                  0x02    0x4C    0xA1    0xD4
CALIB_PATTERN3                                  0x02    0x4D    0xA1    0x13
CALIB_PATTERN4                                  0x02    0x4E    0xA1    0xA1
EXT_WA_DELAY00                                  0x02    0x4F    0x44    0x44
EXT_WA_DELAY01                                  0x02    0x50    0x44    0x44
EXT_WA_DELAY02                                  0x02    0x51    0x44    0x44
EXT_WA_DELAY03                                  0x02    0x52    0x44    0x44
EXT_WA_DELAY04                                  0x02    0x53    0x44    0x44
EXT_WA_DELAY05                                  0x02    0x54    0x44    0x44
EXT_WA_DELAY06                                  0x02    0x55    0x44    0x44
EXT_WA_DELAY07                                  0x02    0x56    0x44    0x44
EXT_WA_DELAY08                                  0x02    0x57    0x44    0x44
EXT_WA_DELAY09                                  0x02    0x58    0x44    0x44
EXT_WA_DELAY10                                  0x02    0x59    0x44    0x44
EXT_WA_DELAY11                                  0x02    0x5A    0x44    0x44
EXT_WA_DELAY12                                  0x02    0x5B    0x44    0x44
EXT_WA_DELAY13                                  0x02    0x5C    0x44    0x44
EXT_WA_DELAY14                                  0x03    0x60    0x44    0x44
EXT_WA_DELAY15                                  0x03    0x61    0x44    0x44
EXT_WA_DELAY16                                  0x03    0x62    0x44    0x44
EXT_WA_DELAY17                                  0x03    0x63    0x44    0x44
EXT_WA_DELAY18                                  0x03    0x64    0x44    0x44
EXT_WA_DELAY19                                  0x03    0x65    0x44    0x44
EFUSEMODE                                       0x03    0x67    0x00    0x00
*--------------------------------------------------------------------------------
* PhyPort registers
*--------------------------------------------------------------------------------
PHY_PORT_CONFIG                                 0x00    0x0D    0x01    0x03
scDllCurrentSet0                                0x00    0x01    0x00    0x55
scDllCurrentSet1                                0x00    0x02    0x00    0x55
scDllCurrentSet2                                0x00    0x03    0x00    0x55
scDllConfirmCountSelect0                        0x00    0x04    0x55    0xFF
scDllConfirmCountSelect1                        0x00    0x05    0x55    0xFF
scDllConfirmCountSelect2                        0x00    0x06    0x55    0xFF
scDllResetReq0                                  0x00    0x07    0x00    0x00
scDllResetReq1                                  0x00    0x08    0x00    0x00
scDllCoarseLockDetection0                       0x00    0x09    0x00    0x00
scDllCoarseLockDetection1                       0x00    0x0A    0x00    0x00
scDllnitSMForceClockEnable0                     0x00    0x0B    0x00    0x00
scDllnitSMForceClockEnable1                     0x00    0x0C    0x00    0x00
scEnableLine0                                   0x00    0x0E    0xFF    0xFF
scEnableLine1                                   0x00    0x0F    0xFF    0xFF
scEnableLine2                                   0x00    0x10    0xFF    0xFF
scEnableLine3                                   0x00    0x11    0xFF    0xFF
scEnableLine4                                   0x00    0x12    0xFF    0xFF
scEnableLine5                                   0x00    0x13    0xFF    0xFF
scResetChannels0                                0x00    0x14    0x00    0x00
scResetChannels1                                0x00    0x15    0x00    0x00
scTrainLine0                                    0x00    0x16    0x00    0x00
scTrainLine1                                    0x00    0x17    0x00    0x00
scTrainLine2                                    0x00    0x18    0x00    0x00
scTrainLine3                                    0x00    0x19    0x00    0x00
scTrainLine4                                    0x00    0x1A    0x00    0x00
scTrainLine5                                    0x00    0x1B    0x00    0x00
scAnalogBypass0                                 0x00    0x1C    0x00    0x00
scAnalogBypass1                                 0x01    0x20    0x00    0x00
scAnalogBypass2                                 0x01    0x21    0x00    0x00
scAnalogBypass3                                 0x01    0x22    0x00    0x00
scAnalogBypass4                                 0x01    0x23    0x00    0x00
scAnalogBypass5                                 0x01    0x24    0x00    0x00
scPhaseSelectB0i0                               0x01    0x25    0x66    0x00
scPhaseSelectB0i1                               0x01    0x26    0x66    0x00
scPhaseSelectB0i2                               0x01    0x27    0x66    0x00
scPhaseSelectB0i3                               0x01    0x28    0x66    0x00
scPhaseSelectB0i4                               0x01    0x29    0x66    0x00
scPhaseSelectB0i5                               0x01    0x2A    0x66    0x00
scPhaseSelectB1i0                               0x01    0x2B    0x66    0x00
scPhaseSelectB1i1                               0x01    0x2C    0x66    0x00
scPhaseSelectB1i2                               0x01    0x2D    0x66    0x00
scPhaseSelectB1i3                               0x01    0x2E    0x66    0x00
scPhaseSelectB1i4                               0x01    0x2F    0x66    0x00
scPhaseSelectB1i5                               0x01    0x30    0x66    0x00
scPhaseSelectB2i0                               0x01    0x31    0x66    0x00
scPhaseSelectB2i1                               0x01    0x32    0x66    0x00
scPhaseSelectB2i2                               0x01    0x33    0x66    0x00
scPhaseSelectB2i3                               0x01    0x34    0x66    0x00
scPhaseSelectB2i4                               0x01    0x35    0x66    0x00
scPhaseSelectB2i5                               0x01    0x36    0x66    0x00
scPhaseSelectB3i0                               0x01    0x37    0x66    0x00
scPhaseSelectB3i1                               0x01    0x38    0x66    0x00
scPhaseSelectB3i2                               0x01    0x39    0x66    0x00
scPhaseSelectB3i3                               0x01    0x3A    0x66    0x00
scPhaseSelectB3i4                               0x01    0x3B    0x66    0x00
scPhaseSelectB3i5                               0x01    0x3C    0x66    0x00
*--------------------------------------------------------------------------------
* Clock Tree configuration  
*--------------------------------------------------------------------------------
CLKTREE_CONFIG                                  0x03    0x66    0x00    0x50
*--------------------------------------------------------------------------------
* Mask Registers - determines which bits are refreshed by the new value
*--------------------------------------------------------------------------------
MASK_BLOCK0                                     0x00    0x1D    0x00    0xFF
MASK_BLOCK1                                     0x01    0x3D    0x00    0xFF
MASK_BLOCK2                                     0x02    0x5D    0x00    0xFF
MASK_BLOCK3                                     0x03    0x7D    0x00    0xFF
*--------------------------------------------------------------------------------
* SEU Counters (Read only)
*--------------------------------------------------------------------------------
ASYNC_CNTL_BLOCK0                             0x00    0x1E    0x00    0x00
ASYNC_CNTL_BLOCK1                             0x00    0x3E    0x00    0x00
ASYNC_CNTL_BLOCK2                             0x00    0x5E    0x00    0x00
ASYNC_CNTL_BLOCK3                             0x00    0x7E    0x00    0x00
SYNC_CNTL_BLOCK0                              0x00    0x1F    0x00    0x00
SYNC_CNTL_BLOCK1                              0x00    0x3F    0x00    0x00
SYNC_CNTL_BLOCK2                              0x00    0x5F    0x00    0x00
SYNC_CNTL_BLOCK3                              0x00    0x7F    0x00    0x00
*--------------------------------------------------------------------------------
* Read only registers
*--------------------------------------------------------------------------------
scPhaseSelectB0o0                               0x04    0x80    0x00    0x00
scPhaseSelectB0o1                               0x04    0x81    0x00    0x00
scPhaseSelectB0o2                               0x04    0x82    0x00    0x00
scPhaseSelectB0o3                               0x04    0x83    0x00    0x00
scPhaseSelectB0o4                               0x04    0x84    0x00    0x00
scPhaseSelectB0o5                               0x04    0x85    0x00    0x00
scPhaseSelectB1o0                               0x04    0x86    0x00    0x00
scPhaseSelectB1o1                               0x04    0x87    0x00    0x00
scPhaseSelectB1o2                               0x04    0x88    0x00    0x00
scPhaseSelectB1o3                               0x04    0x89    0x00    0x00
scPhaseSelectB1o4                               0x04    0x8A    0x00    0x00
scPhaseSelectB1o5                               0x04    0x8B    0x00    0x00
scPhaseSelectB2o0                               0x04    0x8C    0x00    0x00
scPhaseSelectB2o1                               0x04    0x8D    0x00    0x00
scPhaseSelectB2o2                               0x04    0x8E    0x00    0x00
scPhaseSelectB2o3                               0x04    0x8F    0x00    0x00
scPhaseSelectB2o4                               0x04    0x90    0x00    0x00
scPhaseSelectB2o5                               0x04    0x91    0x00    0x00
scPhaseSelectB3o0                               0x04    0x92    0x00    0x00
scPhaseSelectB3o1                               0x04    0x93    0x00    0x00
scPhaseSelectB3o2                               0x04    0x94    0x00    0x00
scPhaseSelectB3o3                               0x04    0x95    0x00    0x00
scPhaseSelectB3o4                               0x04    0x96    0x00    0x00
scPhaseSelectB3o5                               0x04    0x97    0x00    0x00
scDllInstantLock0                               0x04    0x98    0x00    0x00
scDllInstantLock1                               0x04    0x99    0x00    0x00
scDllLocked0                                    0x04    0x9A    0x00    0x00
scDllLocked1                                    0x04    0x9B    0x00    0x00
scChannelLocked0                                0x05    0xA0    0x00    0x00
scChannelLocked1                                0x05    0xA1    0x00    0x00
scChannelLocked2                                0x05    0xA2    0x00    0x00
scChannelLocked3                                0x05    0xA3    0x00    0x00
scChannelLocked4                                0x05    0xA4    0x00    0x00
scChannelLocked5                                0x05    0xA5    0x00    0x00
timingStatusBits                                0x05    0xA6    0x00    0x00
BX0_DELAY                                       0x05    0xA7    0x00    0x00
WA_DELAY00                                      0x05    0xA8    0x00    0x00
WA_DELAY01                                      0x05    0xA9    0x00    0x00
WA_DELAY02                                      0x05    0xAA    0x00    0x00
WA_DELAY03                                      0x05    0xAB    0x00    0x00
WA_DELAY04                                      0x05    0xAC    0x00    0x00
WA_DELAY05                                      0x05    0xAD    0x00    0x00
WA_DELAY06                                      0x05    0xAE    0x00    0x00
WA_DELAY07                                      0x05    0xAF    0x00    0x00
WA_DELAY08                                      0x05    0xB0    0x00    0x00
WA_DELAY09                                      0x05    0xB1    0x00    0x00
WA_DELAY10                                      0x05    0xB2    0x00    0x00
WA_DELAY11                                      0x05    0xB3    0x00    0x00
WA_DELAY12                                      0x05    0xB4    0x00    0x00
WA_DELAY13                                      0x05    0xB5    0x00    0x00
WA_DELAY14                                      0x05    0xB6    0x00    0x00
WA_DELAY15                                      0x05    0xB7    0x00    0x00
WA_DELAY16                                      0x05    0xB8    0x00    0x00
WA_DELAY17                                      0x05    0xB9    0x00    0x00
WA_DELAY18                                      0x05    0xBA    0x00    0x00
WA_DELAY19                                      0x05    0xBB    0x00    0x00
*--------------------------------------------------------------------------------
* Fused ID specific (Read only)
*--------------------------------------------------------------------------------
EFUSEMODE                                       0x03    0x67    0x00    0x00
EfuseValue0                                     0x05    0xBC    0x00    0x00
EfuseValue1                                     0x05    0xBD    0x00    0x00
EfuseValue2                                     0x05    0xBE    0x00    0x00
EfuseValue3                                     0x05    0xBF    0x00    0x00
