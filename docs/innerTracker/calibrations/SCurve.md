# Scurve - Threshold scan

**Scan command:** `scurve`

![Setup](images/scurve_scan1.png){ width=600 }

## Configuration parameters

| Name             | Typical value | Description |
| ---------------- | ------------- | ----------- |
| `nEvents`        | 100           |             |
| `nEvtsBurst`     | =`nEvents`    |             |
| `nTRIGxEvent`    | 10            |             |
| `INJtype`        | 1             |             |
| `VCalHStart`     | 100           |             |
| `VCalHStop`      | 600           |             |
| `VCalHnsteps`    | 50            |             |

## Typical output

### "Threshold 1D"

![Setup](images/scurve_scan2.png){ width=400 }

### "Threshold map"

![Setup](images/scurve_scan3.png){ width=400 }

### "Noise 1D"

![Setup](images/scurve_scan4.png){ width=400 }

### "Noise map"

![Setup](images/scurve_scan5.png){ width=400 }
