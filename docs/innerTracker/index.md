# Inner Tracker overview

1. [Introduction](Intro.md)
1. [FC7 setup](FC7setup.md)
1. [Firmware setup](FWsetup.md)
1. [Software setup](SWsetup.md)
1. [FPGA configuration](FPGAcfg.md)
1. [Configuration file](ConfigFile.md)
1. [Voltage settings](VoltageSettings.md)

For calibrations, see [IT Calibrations](calibrations/index.md).
